#!/usr/bin/pwsh
#

# Opt out of VMware Customer Experience Improvement Program
#Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false | Out-Null

# Ignore certificate warnings
#Set-PowerCLIConfiguration -InvalidCertificateAction "Ignore" -Confirm:$false | Out-Null

# Assemble the HTML Header and CSS for our Report
$HTMLHeader = @"
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html><head><title>My Systems Report</title>
<style type="text/css">
<!--
body {
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}

    #report { width: 835px; }

    table{
    border-collapse: collapse;
    border: none;
    font: 10pt Verdana, Geneva, Arial, Helvetica, sans-serif;
    color: black;
    margin-bottom: 10px;
}

    table td{
    font-size: 12px;
    padding-left: 0px;
    padding-right: 20px;
    text-align: left;
}

    table th {
    font-size: 12px;
    font-weight: bold;
    padding-left: 0px;
    padding-right: 20px;
    text-align: left;
}

h2{ clear: both; font-size: 130%; }

h3{
    clear: both;
    font-size: 115%;
    margin-left: 20px;
    margin-top: 30px;
}

p{ margin-left: 20px; font-size: 12px; }

table.list{ float: left; }

    table.list td:nth-child(1){
    font-weight: bold;
    border-right: 1px grey solid;
    text-align: right;
}

table.list td:nth-child(2){ padding-left: 7px; }
table tr:nth-child(even) td:nth-child(even){ background: #CCCCCC; }
table tr:nth-child(odd) td:nth-child(odd){ background: #F2F2F2; }
table tr:nth-child(even) td:nth-child(odd){ background: #DDDDDD; }
table tr:nth-child(odd) td:nth-child(even){ background: #E5E5E5; }
div.column { width: 320px; float: left; }
div.first{ padding-right: 20px; border-right: 1px  grey solid; }
div.second{ margin-left: 30px; }
table{ margin-left: 20px; }
-->
</style>
</head>
<body>

"@

#function Write-Log
#{
#    Param ([string]$LogString)
#    $LogFile = $vcServer + "_BackupsTagging_" + (Get-Date -UFormat "%d-%b-%Y-%H-%M") + ".log"
#    $DateTime = "[{0:MM/dd/yy} {0:HH:mm:ss}]" -f (Get-Date)
#    $LogMessage = "$Datetime $LogString"
#    Add-content $LogFile -value $LogMessage
#}

function Get-VCList
{
	param
	(
		[Parameter()]
		$file
	)
	
	Get-Content $file
}

function Get-Creds
{
	param
	(
		[Parameter(Mandatory)]
		[string]$credfile
	)
	
	$vcUsername = (Import-CliXml $credfile).UserName
	$vcPassword = (Import-CliXml $credfile).Password
	$vcCredential = New-Object System.Management.Automation.PSCredential ($vcUsername, $vcPassword)
	
	Return $vcCredential
		
}

function Connect-VC
{
	param
	(
		[Parameter(Mandatory, ValueFromPipeLine)]
		[string]$vcServer,
		
		[Parameter(Mandatory=$false)]
		[ValidateNotNull()]
	        [System.Management.Automation.PSCredential] 
			$vcCredential = [System.Management.Automation.PSCredential]::Empty
	)
	
	# Establish vCenter connection with user credentials
 	Write-Host "Connecting to $vcServer as"$vcCredential.Username"..." -Foregroundcolor "Yellow" -NoNewLine

	# Write-Log "Connecting to $vcServer as ""$vcUsername""..." -NoNewLine
	$connection = Connect-VIServer -Server $vcServer -Credential $vcCredential #-ErrorAction SilentlyContinue -WarningAction 0
	# If successful, execute operations
	If ($connection) 
	{
        	Write-Host "Success!`n" -Foregroundcolor "Green"
	}
	# If connection to vCenter wasn't successful, tell me why
	Else
	{
        	Write-Host "Error connecting to $vcServer; Try again with correct user name & password!" -Foregroundcolor "Red"
	}
	
}

function Disconnect-VC
{
	param
	(
		[Parameter()]
		[string]$vcServer
	)
	# Disconnect from vCenter server
	Disconnect-VIServer -Server * -Confirm:$false
	Write-Host "Disconnected from $vcServer." -Foregroundcolor "Yellow"
}

function Get-VMData
{
	param
	(
		[Parameter()]
		$notifyToggle
	)
	$vmdata = @()
	# notifyToggle used to determine type of data collection
	if ($notifyToggle) {
		$date = Get-Date -Format "MM/dd/yyyy HH:mm:ss"
		$vmdata = Get-VM | Where-Object { ($_.Name -cnotlike "vCLS*") -and ($_.Name -cnotmatch ".*-md-\d.*") -and ($_.Name -cnotmatch "cohesity-dummy-archive*") -and ($_.Name -cnotlike "*control-plane*") } | Select-Object Name, @{N='PowerState';E={$_.PowerState -as [string]}}, @{N="BackupTag";E={(Get-TagAssignment -Category "Cohesity Backup Policies" -Entity $_).Tag.Name}}, @{N='vCenter';E={$_.Uid.Substring($_.Uid.IndexOf('@')+1).Split(":")[0]}}, @{N='Date'; E={$date}}, @{N="Found"; E={"false"}}
	} else {
		$vmdata = Get-VM | Where-Object { ($_.Name -cnotlike "vCLS*") -and ($_.Name -cnotmatch ".*-md-\d.*") -and ($_.Name -cnotmatch "cohesity-dummy-archive*") -and ($_.Name -cnotlike "*control-plane*") } | Select-Object Name, PowerState, @{N="BackupTag";E={(Get-TagAssignment -Category "Cohesity Backup Policies" -Entity $_).Tag.Name}}, @{N='vCenter';E={$_.Uid.Substring($_.Uid.IndexOf('@')+1).Split(":")[0]}}
	}

	Return $vmdata

}

function Get-NoTagVMs
{
	param
	(
		[Parameter()]
		$vmlist = $allvms
	)
	
	$vmlist | Where-Object { $null -eq $_.BackupTag } | Sort-Object -Property vCenter,Name
}

function Get-TaggedVMs
{
	param
	(
		[Parameter()]
		$vmlist = $allvms
	)
	
	$vmlist | Where-Object { $null -ne $_.BackupTag } | Sort-Object -Property vCenter,Name
}

function Get-OffDaily
{
	param
	(
		[Parameter()]
		$vmlist = $allvms
	)
	
	$vmlist | Where-Object { ($_.PowerState -Eq "PoweredOff") -And ($_.BackupTag -NotLike "*Archive*") -And ($null -Ne $_.BackupTag) -And ($_.BackupTag -Ne "Backup Disabled") } | Sort-Object -Property vCenter,Name
}

function Get-OnArchive
{
	param
	(
		[Parameter()]
		$vmlist = $allvms
	)

	$vmlist | Where-Object { ($_.PowerState -Ne "PoweredOff") -And ($_.BackupTag -Like "*Archive*") -And ($_.BackupTag -Ne "Backup Disabled") } | Sort-Object -Property vCenter,Name
}

function Get-OffArchive
{
	param 
	(
		[Parameter()]
		$vmlist = $allvms
	)

	$vmlist | Where-Object { ($_.PowerState -Eq "PoweredOff") -And ($_.BackupTag -Like "*Archive*") -And ($_.BackupTag -Ne "Backup Disabled") } | Sort-Object -Property vCenter,Name
}

# Comapre the data on the file with the VM data from the server
function dataCompare {
	param (
		[Parameter()] [Object[]] $fileData,
		[Parameter()] [Object] $buElement
	)
	foreach($fileElement in $fileData) {
		$buName = $buElement.Name
		$fileName = $fileElement.Name
		
		# checks if the VM data is already on the file, returning true if it is
		if($buName -eq $fileName) {
			Write-Host "Names are the same" -ForegroundColor "Green"
			Write-Host "buName: $buName ||| fileName: $fileName`n"
			# $fileElement.Found = "true"
			return $true, $fileElement
		}
	}
	return $false, ""
}

# a function that pulls in file data and a backup element, compares if the backup element exists on the file, if found, it compares the file element age with the backup element age
function checkDataAge {
	param (
		[Parameter()] [Object]$buElement,
		[Parameter()] [Object[]]$fileData,
		[Parameter()] [int]$gracePeriod
		
	)
		# compares the VM data with fileData
		$results, $fileElement = dataCompare $fileData $buElement
		$buName = $buElement.Name
		$fileName = $fileElement.Name
		# enters if found same names in file and in VM data
		if ($results) {
			Write-Host "Check data age" -ForegroundColor "Green"
			Write-Host "buName: $buName ||| fileName: $fileName`n"
			$buAge = ([datetime]$buElement.Date).DayOfYear
			$fileAge = ([datetime]$fileElement.Date).DayOfYear
			$subAge = $buAge - $fileAge
			Write-Host "buAge: $buAge ||| fileAge: $fileAge ||| subAge: $subAge`n"
			# compares the age of the VM data with file data age, making $fileElement.Found = "false" prepares the element to be removed
			if (($subAge -ge $gracePeriod -or $subAge -lt 0) -and $buElement.BackupTag -eq $fileElement.BackupTag) {
				Write-Host "Old Data, Report in Email`n" -ForegroundColor "Red"
				$fileElement.Found = "true"
				return $fileElement, $true
			}
		# VM data was not found on file, and adds it to $fileData
		} else {
			Write-Host "Add data to File" -ForegroundColor "Green"
			Write-Host "buName: $buName`n"
			$buElement.Found = "true"
			return $buElement, $false
		}
}

# notifyToggle is a feature that allows users to determine if they want a grace period for VMs that are Off and have Backup Daily tag before reporting to the server
$notifyToggle = $args[0]
$gracePeriod = 0
$vclistfilename = "vcserverlist.txt"
$vclistfile = Join-Path $PSScriptRoot $vclistfilename
#$vclist = @(Get-VCList -file vcserverlist.txt)
$vclist = @(Get-VCList -file $vclistfile)
$credfilename = "bkuptag.service_automator.xml"
#$credfile = "bkuptag.service_automator.xml"
$credfile = Join-Path $PSScriptRoot $credfilename
$vcCreds = Get-Creds $credfile
$vmdata = @()
$allvms = @()
$notagvms = @()
$onarchivevms = @()
$offnotarchive = @()
$offarchive = @()
$problemONAVms = @()
$problemOAVms = @()
$fromaddress = "<insert email here>"
[string[]]$toaddress = "<insert email here>", "<insert email here>"
$smtpserver = "<insert email here>"
$notagheader = "No Backup Tags Applied"
$wrongtagheader1 = "Wrong Tag for Powered On VM"
$wrongtagheader2 = "Wrong Tag for Powered Off VM"

foreach ($vc in $vclist)
{
	Connect-VC $vc $vcCreds
	
	$vmdata = Get-VMData $notifyToggle

	#$vmdata | Sort-Object -Property Name | Format-Table -AutoSize Name,PowerState,Backup Tag,vCenter

	$allvms += $vmdata
	

	Disconnect-VC -vcServer $vc
}

# references the storage file data.json and reads any present content into fileData array
$filePath = Join-Path $PSScriptRoot "data.json"
$fileData = @()
if (Test-Path -Path $filePath) {
	$fileData += Get-Content -Path $filePath | ConvertFrom-Json
}

$notagvms = Get-NoTagVMs
$onarchivevms = Get-OnArchive
$offnotarchive = Get-OffDaily
$offarchive = Get-OffArchive

if($notifyToggle) {
	# loops through VM data where the VM is Off and not archived/disabled
	foreach($offNotArchiveElement in $offnotarchive) {
		$gracePeriod = 7 # gracePeriod is configurable
		$returnElement, $oldData = checkDataAge $offNotArchiveElement $fileData $gracePeriod # returns the results of the age comparison with the quantifier oldData
		if ($oldData) {
			$problemONAVms += $returnElement # adding to "problemOffNotArchivedVms"
		} elseif (!$oldData -and ($null -ne $returnElement)) {
			$fileData += $returnElement # adding new elements to fileData
		}
	}

	# loops through VM data where the VM is Off and archived/disabled, but not deleted
	foreach($offArchiveElement in $offarchive) {
		$gracePeriod = 14 # gracePeriod is configurable
		$returnElement, $oldData = checkDataAge $offArchiveElement $fileData $gracePeriod # returns the results of the age comparison with the quantifier oldData
		if ($oldData) {
			$problemOAVms += $returnElement	# adding to "problemOffArchivedVms"
		} elseif (!$oldData -and ($null -ne $returnElement)) {
			$fileData += $returnElement # adding new elements to fileData
		}
	}

	$temp = @()
	# loop through $fileData array
	foreach($element in $fileData) {
		$name = $element.Name
		# if $element.Found = "true" add it to the temp array, this allows for elements where Found = "false" to be removed
	 	if($element.Found -eq "true") {
			$element.Found = "false"				# reset Found value for next run
			$temp += $element						# add Found = "true" elems to temp array
	 	} else {
	 		Write-Host "Removing content from File" -ForegroundColor "Red"
			Write-Host "Name: $name`n"
		}
		# set $fileData = $temp removing all Found = "false" elements
		$fileData = $temp
	 }
	$fileData | ConvertTo-Json | Out-File $filePath	# write data to data.json file, creating the file if it does not exist
}

$HTMLMiddle = $null

If (($notagvms | Measure-Object).Count -gt 0)
{   
	$notagvmshtml = ($notagvms | ConvertTo-HTML -Fragment)
	$CurrentHTML = @"
	<h3>$notagheader</h3>
	<p>Server(s) listed below do not have a backup tag applied. Servers with a backup tag will not be listed.</p>
	$notagvmshtml
	<br></br>
"@
	$HTMLMiddle += $CurrentHTML
}

If (($onarchivevms | Measure-Object).Count -gt 0)
{
	$onarchivevmshtml = ($onarchivevms | ConvertTo-HTML -Fragment)
	$CurrentHTML = @"
	<h3>$wrongtagheader1</h3>
	<p>Server(s) listed below are powered on, but have an archive tag applied. Servers with the correct tag will not be listed.</p>
	$onarchivevmshtml
	<br></br>
"@
	$HTMLMiddle += $CurrentHTML
}

# changes email format depending on notifyToggle state
If ($notifyToggle -and (($problemONAVms | Measure-Object).Count -gt 0)) {
	Write-Host "Converting data to email" -ForegroundColor "Green"
	$fileDataHTML = ($problemONAVms | ConvertTo-HTML -Fragment)
	$CurrentHTML = @"
	<h3>$wrongtagheader2</h3>
	<p>Server(s) listed below are powered off, and have a Backup Daily tag applied. Servers with the correct tag will not be listed.
	$fileDataHTML
	<br><br>
"@
	$HTMLMiddle += $CurrentHTML
} elseIf (!$notifyToggle -and ($offnotarchive | Measure-Object).Count -gt 0) {
	$offnotarchivehtml = ($offnotarchive | ConvertTo-HTML -Fragment)
	$CurrentHTML = @"
	<h3>$wrongtagheader2</h3>
	<p>Server(s) listed below are powered off, but do not have an archive tag applied. Servers with the correct tag will not be listed.
	$offnotarchivehtml
	<br></br>
"@
	$HTMLMiddle += $CurrentHTML
}

# add problem Off and Archived VMs to the email
If ($notifyToggle -and (($problemOAVms | Measure-Object).Count -gt 0)) {
	Write-Host "Converting data to email" -ForegroundColor "Green"
	$fileDataHTML = ($problemOAVms | ConvertTo-HTML -Fragment)
	$CurrentHTML = @"
	<h3>$wrongtagheader2</h3>
	<p>Server(s) listed below are powered off for 14 days, and have an Archive tag applied. Check if these Servers need to be Deleted.
	$fileDataHTML
	<br><br>
"@
	$HTMLMiddle += $CurrentHTML
}

# Assemble the closing HTML for our report.
$HTMLEnd = @"
</div>
</body>
</html>
"@

# If HTMLMiddle isn't empty/null, then compile the HTML message and send an e-mail.  Otherwise, nothing is sent.
If ($HTMLMiddle)
{
	# Assemble the final report from all our HTML sections
	$HTMLmessage = $HTMLHeader + $HTMLMiddle + $HTMLEnd
	# Email our report out
	Send-MailMessage -WarningAction:SilentlyContinue -from $fromaddress -to $toaddress -subject "Backup Tags Report" -BodyAsHTML -body $HTMLmessage -priority Normal -smtpServer $smtpserver
}
